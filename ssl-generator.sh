#!/usr/bin/env sh
# Author: thdelmas
# Generate self-signed ssl certificates


target_directory=/etc/ssl/certs
usage='ssl-generator [out_name [ target_directory ]]'

echo $usage >&2

if [ "$1" ]; then
	target_name="$1"
else
	target_name=server
fi

if [ "$2" ]; then
	if [ ! -d "$2" ]; then
		echo "$2: is not a diectory" >&2
		exit 1
	fi
	if [ ! -w "$2" ]; then
		echo "$2: Permission denied" >&2
		exit 1
	fi
	target_directory="$2"
fi

if ! which openssl ; then
	echo 'openssl not found.'
	exit 1
fi

openssl genrsa -des3 -out "$target_name.key" 1024
openssl req -new -key "$target_name.key" -out "$target_name.csr"
cp "$target_name.key" "$target_name.key.org"
openssl rsa -in "$target_name.key.org" -out "$target_name.key"
openssl x509 -req -days 365 -in "$target_name.csr" -signkey "$target_name.key" -out "$target_name.crt"
cp -v "$target_name.crt" "$target_direcory/$target_name.crt"
cp -v "$target_name.key" "$target_direcory/$target_name.key"
